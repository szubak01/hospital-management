package com.example.demo.model;


import com.example.demo.model.enums.Gender;
import com.example.demo.model.enums.Role;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/** Entity that represents user.
 * User has 4 roles: Admin, Doctor, Nurse, Receptionist.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class User {

  public User(
      Long id,
      String firstName,
      String lastName,
      String login,
      String password,
      Gender gender,
      Role role) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.login = login;
    this.password = password;
    this.gender = gender;
    this.role = role;
  }

  /** Creates user with specified first name, last name, login, password, gender, role.
   * @param firstName User's first name.
   * @param lastName User's last name.
   * @param login User's login.
   * @param password User's password
   * @param gender User's gender.
   * @param role User's role in the system.
   */
  public User(String firstName,
      String lastName,
      String login,
      String password,
      Gender gender,
      Role role) {

    this.firstName = firstName;
    this.lastName = lastName;
    this.login = login;
    this.password = password;
    this.gender = gender;
    this.role = role;
  }

  public User(
      String login,
      String password,
      Role role) {
    this.login = "admin";
    this.password = "pass";
    this.role = Role.ADMIN;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String firstName;

  private String lastName;

  private String login;

  private String password;

  private Gender gender;

  private Role role;

  //foreign key for Injection (doctor)
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "doctor")
  private List<Injection> doctorInjectionList;

  //foreign key for Injection (nurse)
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "nurse")
  private List<Injection> nurseInjectionList;

  // foreign key for Appointment (doctor)
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "doctor")
  private List<Appointment> doctorAppList;

  // foreign key for Prescription (doctor)
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "doctor")
  private List<Prescription> doctorPreList;

}
