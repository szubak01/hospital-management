package com.example.demo;


import com.example.demo.model.Injection;
import com.example.demo.model.Patient;
import com.example.demo.model.User;
import com.example.demo.model.enums.Gender;
import com.example.demo.model.enums.InjectionStatus;
import com.example.demo.model.enums.Role;
import com.example.demo.repository.AppointmentRepository;
import com.example.demo.repository.InjectionRepository;
import com.example.demo.repository.PatientRepository;
import com.example.demo.repository.UserRepository;
import java.time.LocalDate;
import java.time.Month;
import java.util.Random;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class MyRunner implements CommandLineRunner {

  private static final Logger logger = LoggerFactory.getLogger(MyRunner.class);

  @Autowired private final UserRepository userRepo;
  @Autowired private final PatientRepository patientRepo;
  @Autowired private final AppointmentRepository appointmentRepo;
  @Autowired private final InjectionRepository injectionRepo;
  //@Autowired private final PrescriptionRepository prescriptionRepo;

  @Override
  public void run(String... args) throws Exception {

    // USER
    userRepo.deleteAll();
    patientRepo.deleteAll();
    injectionRepo.deleteAll();
   // prescriptionRepo.deleteAll();
    appointmentRepo.deleteAll();


    // ADMIN
    userRepo.save(new User("null", "null", "admin", "pass", Gender.MALE, Role.ADMIN));


    User doctor1 = new User("Thomas", "Hunt", "ThomasHunt", "password", Gender.MALE, Role.DOCTOR); userRepo.save(doctor1);
    User doctor2 = new User("Ethan", "Johnson", "EthanJohnson", "password", Gender.MALE, Role.DOCTOR); userRepo.save(doctor2);
    User doctor3 = new User("Jacob", "Mason", "JacobMason", "password", Gender.MALE, Role.DOCTOR); userRepo.save(doctor3);
    User doctor4 = new User("Mia", "Owens", "MiaOwens", "password", Gender.FEMALE, Role.DOCTOR); userRepo.save(doctor4);
    userRepo.save(new User("Madison", "Parker", "MadisonParker", "password", Gender.FEMALE, Role.RECEPTIONIST));
    User nurse1 = new User("Grace", "Scott", "GraceScott", "password", Gender.FEMALE, Role.NURSE); userRepo.save(nurse1);
    User nurse2 = new User("Kelly", "Moore", "KellyMoore", "password", Gender.FEMALE, Role.NURSE); userRepo.save(nurse2);

    // PATIENT


    LocalDate dayOfBirth = LocalDate.of(1990, Month.JANUARY, 1);
    Random rand = new Random(System.currentTimeMillis());
    String[] phoneNumber = new String[10];
    for (int i = 0; i <= phoneNumber.length - 1; i++) {
      Long drand = (long)(rand.nextInt());
      String sNumber = drand.toString();
      phoneNumber[i] = sNumber;
    }

    Patient patient1 = new Patient("John", "Doe", dayOfBirth, Gender.MALE, "New York", phoneNumber[0]);  patientRepo.save(patient1);
    Patient patient2 = new Patient("Oliver", "Adams", dayOfBirth, Gender.MALE, "New Jersey", phoneNumber[1]); patientRepo.save(patient2);
    Patient patient3 = new Patient("George", "Atkinson", dayOfBirth, Gender.MALE, "New York", phoneNumber[2]); patientRepo.save(patient3);
    Patient patient4 = new Patient("Harry", "Baker", dayOfBirth, Gender.MALE, "New York", phoneNumber[3]); patientRepo.save(patient4);
    Patient patient5 = new Patient("Jack", "Brown", dayOfBirth, Gender.MALE, "New Jersey", phoneNumber[4]); patientRepo.save(patient5);
    Patient patient6 = new Patient("Jacob", "Burton", dayOfBirth, Gender.MALE, "New Jersey", phoneNumber[5]); patientRepo.save(patient6);
    Patient patient7 = new Patient("Ashley", "Carter", dayOfBirth, Gender.FEMALE, "New York", phoneNumber[6]); patientRepo.save(patient7);
    Patient patient8 = new Patient("Emma", "Fisher", dayOfBirth, Gender.FEMALE, "New York", phoneNumber[7]); patientRepo.save(patient8);
    Patient patient9 = new Patient("Sophia", "Fletcher", dayOfBirth, Gender.FEMALE, "New Jersey", phoneNumber[8]); patientRepo.save(patient9);
    Patient patient10 = new Patient("Charlotte", "Lewis", dayOfBirth, Gender.FEMALE, "New Jersey", phoneNumber[9]); patientRepo.save(patient10);


    // INJECTION

    LocalDate injectionDate = LocalDate.now();

    injectionRepo.save(new Injection("example description 1", injectionDate, InjectionStatus.PENDING, doctor1, nurse1, patient1));
    injectionRepo.save(new Injection("example description 1", injectionDate, InjectionStatus.PENDING, doctor1, nurse1, patient1));
    injectionRepo.save(new Injection("example description 1", injectionDate, InjectionStatus.PENDING, doctor1, nurse1, patient1));
    injectionRepo.save(new Injection("example description 1", injectionDate, InjectionStatus.DONE, doctor1, nurse1, patient1));
    injectionRepo.save(new Injection("example description 1", injectionDate, InjectionStatus.DONE, doctor1, nurse1, patient1));

    // APPOINTMENT

/*
    LocalDateTime startDate = LocalDateTime.of(2021, Month.JANUARY, 31, 10,30);
    LocalDateTime endDate = LocalDateTime.of(2021, Month.JANUARY, 31, 10,45);
    LocalDateTime startDate2 = LocalDateTime.of(2021, Month.JANUARY, 31, 11,45);
    LocalDateTime endDate2 = LocalDateTime.of(2021, Month.JANUARY, 31, 12,30);


    appointmentRepo.save(new Appointment(startDate, endDate, false));
    appointmentRepo.save(new Appointment(startDate2, endDate2, false));
    appointmentRepo.save(new Appointment(startDate, endDate, false));
    appointmentRepo.save(new Appointment(startDate2, endDate2, false));
    appointmentRepo.save(new Appointment(startDate, endDate, false));




    // PRESCRIPTION


    prescriptionRepo.save(new Prescription("example description pre", injectionDate));
    prescriptionRepo.save(new Prescription("example description pre2", injectionDate));
    prescriptionRepo.save(new Prescription("example description pre3", injectionDate));
    prescriptionRepo.save(new Prescription("example description pre4", injectionDate));
    prescriptionRepo.save(new Prescription("example description pre5", injectionDate));

 */
  }
}
